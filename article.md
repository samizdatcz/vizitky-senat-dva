---
title: "Než se rozhodnete: Poslechněte si názory kandidátů, kteří postoupili do druhého kola senátních voleb"
perex: "V prvním kole senátních voleb bylo nejúspěšnější hnutí ANO. Jeho kandidáti postoupili do druhého kola ve 14 obvodech se 27. Adepti ČSSD a KDU-ČSL postoupili v devíti obvodech, ODS v pěti. Čtyři želízka v ohni mají Starostové a nezávislí spolu se Starosty pro Liberecký kraj. Návrat do Senátu možná čeká TOP 09 díky třem finalistům."
description: "V prvním kole senátních voleb bylo nejúspěšnější hnutí ANO. Jeho kandidáti postoupili do druhého kola ve 14 obvodech se 27. Adepti ČSSD a KDU-ČSL postoupili v devíti obvodech, ODS v pěti. Čtyři želízka v ohni mají Starostové a nezávislí spolu se Starosty pro Liberecký kraj. Návrat do Senátu možná čeká TOP 09 díky třem finalistům."
authors: ["Regionální stanice ČRo"]
published: "12. října 2016"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
socialimg: https://interaktivni.rozhlas.cz/senat-druhe-kolo/media/socialimg.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "senat-druhe-kolo"
libraries: [jquery, inline-audio]
recommended:
  - link: https://interaktivni.rozhlas.cz/zisky-v-krajskych-volbach/
    title: "Mapa: ČSSD ztrácela napříč republikou, ANO triumfuje"
    perex: "Srovnání zisků jednotlivých stran v letošních krajských volbách ukazuje, že voliči odcházejí od sociálních demokratů k ANO 2011 Andreje Babiše."
    image: https://interaktivni.rozhlas.cz/data/kraj-volby-zisky/www/media/socialimg.jpg
  - link: https://interaktivni.rozhlas.cz/preference-2016/
    title: "Šampioni preferenčních hlasů: Kdo nejvíc získal, kdo nejvíc ztratil?"
    perex: "Sedmačtyřicet nově zvolených krajských zastupitelů získalo mandát jen díky „kroužkování“. Jiných sedmačtyřicet kandidátů ze stejného důvodu o místo v zastupitelstvu přišlo. Podívejte se, kteří to jsou a kterými kandidátkami preferenční hlasy zamíchaly nejvíce."
    image: https://interaktivni.rozhlas.cz/data/volby-2016-preference-text/www/media/socialimg.jpg
  - link: https://interaktivni.rozhlas.cz/preference-2016/
    title: Šampioni preferenčních hlasů: Kdo nejvíc získal, kdo nejvíc ztratil?
    perex: Sedmačtyřicet nově zvolených krajských zastupitelů získalo mandát jen díky „kroužkování“. Jiných sedmačtyřicet kandidátů ze stejného důvodu o místo v zastupitelstvu přišlo. Podívejte se, kteří to jsou a kterými kandidátkami preferenční hlasy zamíchaly nejvíce.
    image: https://interaktivni.rozhlas.cz/preference-2016/media/socialimg.jpg
---

Český rozhlas položil všem kandidátům stejných šest otázek. Na odpovědi měli všichni shodný časový limit. Poslechněte si jejich odpovědi před druhým kolem senátních voleb, které se koná v pátek 14. a v sobotu 15. října.

<aside class="big">
  <div id="vizitky"><ul class="viz"></ul></div>
</aside>

Podoba volebních vizitek je pro kandidáty jednotná. První otázka zní „Proč by lidé měli volit právě vás?“ a na odpověď mají všichni jednu minutu. Časový limit u dalších pěti otázek je 30 sekund. Kandidáti samozřejmě můžou odpovědět v kratším čase, ušetřené sekundy se ale nepřevádějí.

Český rozhlas se tedy dále ptal:

- Jaký nový zákon nebo novelu byste navrhoval jako první?
- Jak byste chtěl/a zvýšit důvěryhodnost Senátu?
- Co je nejpalčivějším problémem vašeho volebního obvodu a jak chcete přispět k jeho řešení?
Co byste konkrétně udělal pro zlepšení politické kultury v České republice, tedy nejen v Senátu?
- Budete se v případě zvolení věnovat senátorské práci naplno, nebo se budete chtít souběžně věnovat i jiným zaměstnaneckým, podnikatelským, politickým nebo dalším aktivitám?

Kandidáti do Senátu sice všechny otázky předem znali, při natáčení ale museli odpovídat z hlavy a na první pokus. Nemohli tedy mít před sebou připravený text ani poznámky na papíře nebo v mobilním telefonu či tabletu. Někteří ale nabídku na natočení volební vizitky odmítli.

Rozhovory s jednotlivými kandidáty se ve vysílání Českého rozhlasu objeví postupně. Pořadí určil los, na který dohlížel notář.

Stejný prostor [nabídl Český rozhlas také lídrům všech krajských kandidátek](https://interaktivni.rozhlas.cz/lidri-krajskych-kandidatek/).